pyinstaller  ^
    --onefile ^
    CarGo.spec
pyinstaller --onefile CarGoSpy.spec
REM --upx-dir=c:\PRoGS\Soft\_Soft\Tools\UPX ^
XCOPY .\wfc_config\TrackerNormal.ui .\dist\wfc_config\ /Y
XCOPY .\wfc_config\TrackerSpy.ui .\dist\wfc_config\ /Y
XCOPY .\wfc_config\images\logo_221.jpg .\dist\wfc_config\images\ /Y
XCOPY .\wfc_config\images\icon.ico .\dist\wfc_config\images\ /Y
REM set dd=%DATE:~0,2%
REM set mm=%DATE:~3,2%
REM set yyyy=%DATE:~6,4%
REM set curdate=%yyyy%-%mm%-%dd%
set file=.\wfc_config\__init__.py
for /F "tokens=2 delims== " %%E IN ('findstr "^__version__" %file%') DO set version=%%E
set version=%version:~1,-1%
set name=CarGo_%version%
7z a -tzip -ssw -mx1  -r0 .\release\%name%.zip .\dist\*
aws s3 cp .\release\%name%.zip s3://storage.ghostku.com/cargo/ --acl public-read
del .\release\%name%.zip
