CONSTANTS = {
    'park_sensivity': 90,
    "setup_kalman": 0,
    "setup_autoreboot": 0,
    "mode_constant_x": 90,
    "mode_motion_x": 90,
    "mode_beacon_y": 300,
    "mode_hide_y": 300
}

def constant(func):
    def wrapper(*args, **kwargs):
        if func.__name__ in CONSTANTS:
            return
        func(*args, **kwargs)
    return wrapper
