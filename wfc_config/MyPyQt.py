# MyPyQt.py
# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring
# pylint: disable=no-name-in-module
# pylint: disable=unused-import

from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout, 
QPushButton)
from PyQt5.QtGui import (QCursor, QPixmap, QIcon)
from PyQt5.QtCore import (Qt, QObject, QThread, pyqtSignal)
from PyQt5.QtWidgets import (QApplication, QButtonGroup, QPushButton, QCheckBox, QComboBox, QLabel, QLineEdit, QMainWindow, QSpinBox, QTextBrowser, QFileDialog, QTimeEdit)