import logging
from wfc_config.MyPyQt import QTextBrowser


class WFCLogFormatter(logging.Formatter):
    err_fmt = '<p style="color:red">ERR: %(msg)s</p>'
    dbg_fmt = '<p style="color:orange">DBG: %(module)s: %(lineno)d: %(msg)s</p>'
    send_fmt = '<p style="color:blue">[&gt;] %(msg)s</p>'
    read_fmt = '<p style="color:green">[&lt;] %(msg)s</p>'

    info_fmt = "%(msg)s"
    console_out = '==> %(msg)s'

    def __init__(self):
        super().__init__(fmt="%(levelno)d: %(msg)s", datefmt=None, style='%')

    def format(self, record):
        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        if record.levelno == logging.DEBUG:
            self._style._fmt = WFCLogFormatter.dbg_fmt
        elif record.levelno == logging.INFO:
            if record.funcName == 'read_from_port':
                self._style._fmt = WFCLogFormatter.read_fmt
            elif record.funcName == 'send_cmd':
                self._style._fmt = WFCLogFormatter.send_fmt
            else:
                self._style._fmt = WFCLogFormatter.info_fmt
        elif record.levelno == logging.ERROR:
            self._style._fmt = WFCLogFormatter.err_fmt
        elif record.levelno == 25:
            self._style._fmt = WFCLogFormatter.console_out
        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig
        return result


class QTextBrowserLogger(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.widget = QTextBrowser(parent)
        self.widget.setReadOnly(True)

    def emit(self, record):
        msg = self.format(record)
        self.widget.append(msg)
        self.widget.ensureCursorVisible()


class WFCLogger(logging.Logger):
    def console_out(self, msg, *args, **kwargs):
        if self.isEnabledFor(25):
            self._log(25, msg, args, **kwargs)