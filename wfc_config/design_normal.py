from wfc_config.design import MainWindow


class NormalWindow(MainWindow):
    STRINGS = {"title": "CarGO WFC Config Tool"}

    def __init__(self, tracker, model, debug):
        super().__init__(tracker, model, debug, "./wfc_config/TrackerNormal.ui")

        # Interfaces
        self.btn_info_interfaces.clicked.connect(self.get_interfaces_info)
        self.btn_info_dut.clicked.connect(self.get_dut_info)

        self.ACTIVE_WHEN_CONNECTED = [
            self.btn_read_all,
            self.btn_write_all,
            self.btn_setup_update,
            self.btn_reset_buffer,
            # self.btn_battery_off,
            self.btn_reset,
            self.btn_restart,
            self.btn_info_common,
            self.btn_send_command,
            self.btn_info_interfaces,
            self.btn_info_dut,
            self.txt_ussd,
            self.btn_ussd,
        ]
