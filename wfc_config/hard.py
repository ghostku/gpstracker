import glob
import io
import re
import serial
import sys
import threading
import json

from datetime import time, datetime
from time import sleep
from wfc_config.crc16 import calcString
from wfc_config import logger
from wfc_config.hard_constants import constant, CONSTANTS

WAIT_BETWEEN_WRITE = 1
WAIT_DELAY = 0.3
WAIT_RESET_DELAY = 2


class TrackerException(Exception):
    """Base class for exceptions in this module."""

    pass


class ValidationError(TrackerException):
    pass


class ConnectionError(TrackerException):
    pass


def show_serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith("win"):
        ports = ["COM%s" % (i + 1) for i in range(256)]
    elif sys.platform.startswith("linux") or sys.platform.startswith("cygwin"):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob("/dev/tty[A-Za-z]*")
    elif sys.platform.startswith("darwin"):
        ports = glob.glob("/dev/tty.*")
    else:
        raise EnvironmentError("Unsupported platform")

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def crc(command):
    crc = calcString(command, 0xFFFF)
    crc = hex(crc)[2:]
    crc = [int(crc[i : i + 2], 16) for i in range(0, len(crc), 2)]
    command = [ord(i) for i in command]
    command += [0] + crc
    # print('Command + 0x00: ', command)
    return command


class Tracker(object):
    """docstring for Tracker"""

    ANSWER_LINES = {"info:conf": 7, "atcmd:at+csq": 3}
    DEBUG_GET = {
        "info:ver": ["V02060219-DEBUG"],
        "info:conf": [
            "SIM: 1",
            "Server: 127.0.0.1:1199",
            "apn1: dsfsdf",
            "apn2: apn2222",
            "Timeget: 60,100,9,1111",
            "Parking: 80,60,0,300",
            "Setup: 001101,200",
        ],
        "atcmd:at+csq": [],
        "pin:": ["pin:0003;"],
        "out:": ["Out=0"],
        "data:": ["data:dut1t;"],
        "mode:": ["mode:hide:t:0/20,18/0,300;"],
        "wl:pn1:;": ["pn1:1"],
        "wl:pn2:;": ["pn2:1"],
        "wl:pn3:;": ["pn3:1"],
        "bt:": ["bt:1"],
        "sim:": ["sim:0,30,0"],
    }
    DEBUG_SET = {}
    FIELDS_TO_SAVE = [
        "active_sim",
        "server_ip",
        "server_port",
        "sim1_apn",
        "sim1_log",
        "sim1_pwd",
        "sim2_apn",
        "sim2_log",
        "sim2_pwd",
        "send_interval",
        "send_distance",
        "send_degree",
        "send_engine",
        "send_digital_input",
        "send_speed",
        "send_fuel",
        "park_sensivity",
        "park_downtime",
        "sleep_downtime",
        "park_interval",
        "setup_autoreboot",
        "setup_kalman",
        "setup_GSM_location",
        "setup_buffer",
        "setup_keep_alive",
        "interfaces_out",
        "setup_data_in1",
        "setup_data_in2",
        "setup_data_out",
        "setup_data_acc",
        "setup_gsm_setter",
        "setup_data_dut1t",
        "setup_data_dut2t",
        "setup_data_adc1",
        "setup_data_pwrext",
        "setu_data_pwrbat",
        "setup_data_oper",
        "setup_data_dut1L",
        "setup_data_dut2L",
        "PIN",
    ]
    ALLOWED_COMMANDS = {
        "normal": [
            "server",
            "sim1",
            "sim2",
            "timeget",
            "parking",
            "setup",
            "out",
            "data",
            "PIN",
            "bluetooth",
            "sim:set",
            "sim:en",
            "sim",
            "active_sim",
        ],
        "spy": [
            "server",
            "sim1",
            "sim2",
            "timeget",
            "parking",
            "setup",
            "out",
            "data",
            "PIN",
            "mode",
            "wl:pn1",
            "wl:pn2",
            "wl:pn3",
            "bluetooth",
            "sim:set",
            "sim:en",
            "sim",
            "active_sim",
        ],
    }  # Remember that 'active_sim' should be the latest in both lists

    MODES = ["constant", "motion", "beacon", "hide:p", "hide:t"]

    def __init__(self, model="normal", debug=False):
        super(Tracker, self).__init__()
        self.model = model
        self.allowed_commands = self.ALLOWED_COMMANDS[self.model]
        self.changed = set()
        self.debug = debug
        self.read_thread = None
        self.connected = False
        self.__admin_phone = ["_", "", "", ""]
        self._mode_calendar_wake = [time(), time(), time()]
        self._mode_calendar_wake_enabled = [False, False, False]
        self.__data = {
            "in1": False,
            "in2": False,
            "out": False,
            "acc": False,
            "gsm": False,
            "dut1t": False,
            "dut2t": False,
            "adc1": False,
            "pwrext": False,
            "pwrbat": False,
            "oper": False,
            "dut1L": False,
            "dut2L": False,
        }
        self.load_defaults()

    def load_defaults_info(self):
        self._info_ver = None

    def load_defaults(self):
        self._setup_GSM_location = False
        self.load_defaults_info()
        self._mode_constant_y = 300
        self._mode_beacon_x = 0
        self._mode_motion_y = 180
        self.mode_hide_x = 0

        # Fill Constants
        for name, value in CONSTANTS.items():
            setattr(self, "_" + name, value)

    # Setters And Getters

    @property  # active_sim
    def active_sim(self):
        return self._active_sim

    @active_sim.setter
    def active_sim(self, value):
        self._active_sim = value
        self.changed.add("active_sim")

    @property  # SIM_autoswitch_onfail_enabled
    def SIM_autoswitch_onfail_enabled(self):
        return self._SIM_autoswitch

    @SIM_autoswitch_onfail_enabled.setter
    def SIM_autoswitch_onfail_enabled(self, value):
        self._SIM_autoswitch = value
        self.changed.add("sim:en")

    @property  # SIM_autoswitch_onfail.value
    def SIM_autoswitch_onfail_value(self):
        return self._SIM_autoswitch_onfail_value

    @SIM_autoswitch_onfail_value.setter
    def SIM_autoswitch_onfail_value(self, value):
        self._SIM_autoswitch_onfail_value = value
        self.changed.add("sim:set")

    @property  # SIM_autoswitch_back_enabled
    def SIM_autoswitch_back_enabled(self):
        return bool(self._SIM_autoswitch_back_value)

    @SIM_autoswitch_back_enabled.setter
    def SIM_autoswitch_back_enabled(self, value):
        if not value:
            self._SIM_autoswitch_back_value = 0
        self.changed.add("sim:set")

    @property  # SIM_autoswitch_back.value
    def SIM_autoswitch_back_value(self):
        return self._SIM_autoswitch_back_value

    @SIM_autoswitch_back_value.setter
    def SIM_autoswitch_back_value(self, value):
        self._SIM_autoswitch_back_value = value
        self.changed.add("sim:set")

    @property  # server_ip
    def server_ip(self):
        return self._server_ip

    @server_ip.setter
    def server_ip(self, value):
        self._server_ip = value
        self.changed.add("server")

    @property  # server_port
    def server_port(self):
        return self._server_port

    @server_port.setter
    def server_port(self, value):
        self._server_port = value
        self.changed.add("server")

    @property  # sim1_apn
    def sim1_apn(self):
        return self._sim1_apn

    @sim1_apn.setter
    def sim1_apn(self, value):
        self._sim1_apn = value
        self.changed.add("sim1")

    @property  # sim1_log
    def sim1_log(self):
        return self._sim1_log

    @sim1_log.setter
    def sim1_log(self, value):
        self._sim1_log = value
        self.changed.add("sim1")

    @property  # sim1_pwd
    def sim1_pwd(self):
        return self._sim1_pwd

    @sim1_pwd.setter
    def sim1_pwd(self, value):
        self._sim1_pwd = value
        self.changed.add("sim1")

    @property  # sim2_apn
    def sim2_apn(self):
        return self._sim2_apn

    @sim2_apn.setter
    def sim2_apn(self, value):
        self._sim2_apn = value
        self.changed.add("sim2")

    @property  # sim2_log
    def sim2_log(self):
        return self._sim2_log

    @sim2_log.setter
    def sim2_log(self, value):
        self._sim2_log = value
        self.changed.add("sim2")

    @property  # sim2_pwd
    def sim2_pwd(self):
        return self._sim2_pwd

    @sim2_pwd.setter
    def sim2_pwd(self, value):
        self._sim2_pwd = value
        self.changed.add("sim2")

    # Timeget

    @property  # send_interval
    def send_interval(self):
        return self._send_interval

    @send_interval.setter
    def send_interval(self, value):
        self._send_interval = value
        self.changed.add("timeget")

    @property  # send_distance
    def send_distance(self):
        return self._send_distance

    @send_distance.setter
    def send_distance(self, value):
        self._send_distance = value
        self.changed.add("timeget")

    @property  # send_degree
    def send_degree(self):
        return self._send_degree

    @send_degree.setter
    def send_degree(self, value):
        self._send_degree = value
        self.changed.add("timeget")

    @property  # send_engine
    def send_engine(self):
        return self._send_engine

    @send_engine.setter
    def send_engine(self, value):
        self._send_engine = value
        self.changed.add("timeget")

    @property  # send_digital_input
    def send_digital_input(self):
        return self._send_digital_input

    @send_digital_input.setter
    def send_digital_input(self, value):
        self._send_digital_input = value
        self.changed.add("timeget")

    @property  # send_speed
    def send_speed(self):
        return self._send_speed

    @send_speed.setter
    def send_speed(self, value):
        self._send_speed = value
        self.changed.add("timeget")

    @property  # send_fuel
    def send_fuel(self):
        return self._send_fuel

    @send_fuel.setter
    def send_fuel(self, value):
        self._send_fuel = value
        self.changed.add("timeget")

    # Parking

    @property  # park_sensivity
    def park_sensivity(self):
        return self._park_sensivity

    @park_sensivity.setter
    @constant
    def park_sensivity(self, value):
        self._park_sensivity = value
        self.changed.add("parking")

    @property  # park_downtime
    def park_downtime(self):
        return self._park_downtime

    @park_downtime.setter
    def park_downtime(self, value):
        self._park_downtime = value
        self.changed.add("parking")

    @property  # sleep_downtime
    def sleep_downtime(self):
        return self._sleep_downtime

    @sleep_downtime.setter
    def sleep_downtime(self, value):
        self._sleep_downtime = value
        self.changed.add("parking")

    @property  # park_interval
    def park_interval(self):
        return self._park_interval

    @park_interval.setter
    def park_interval(self, value):
        self._park_interval = value
        self.changed.add("parking")

    @property  # setup_autoreboot
    def setup_autoreboot(self):
        return self._setup_autoreboot

    @setup_autoreboot.setter
    @constant
    def setup_autoreboot(self, value):
        self._setup_autoreboot = value
        self.changed.add("setup")

    @property  # setup_kalman
    def setup_kalman(self):
        return self._setup_kalman

    @setup_kalman.setter
    @constant
    def setup_kalman(self, value):
        self._setup_kalman = value
        self.changed.add("setup")

    @property  # setup_GSM_location
    def setup_GSM_location(self):
        return self._setup_GSM_location

    @setup_GSM_location.setter
    def setup_GSM_location(self, value):
        self._setup_GSM_location = value
        self.changed.add("setup")

    @property  # setup_buffer
    def setup_buffer(self):
        return self._setup_buffer

    @setup_buffer.setter
    def setup_buffer(self, value):
        self._setup_buffer = value
        self.changed.add("setup")

    @property  # setup_keep_alive
    def setup_keep_alive(self):
        return self._setup_keep_alive

    @setup_keep_alive.setter
    def setup_keep_alive(self, value):
        self._setup_keep_alive = 120 if value else 0
        self.changed.add("setup")

    # Interfaces
    @property  # interfaces_out
    def interfaces_out(self):
        return self._interfaces_out

    @interfaces_out.setter
    def interfaces_out(self, value):
        if str(value) not in ["1", "0", "b"]:
            raise ValidationError('Wrong OUT: "%s" can' "t be interfaces_out" % value)
        self._interfaces_out = value
        self.changed.add("out")

    @property  # bluetooth
    def bluetooth(self):
        return self._bluetooth

    @bluetooth.setter
    def bluetooth(self, value):
        self._bluetooth = value
        self.changed.add("bluetooth")

    # Mode data
    @property  # mode
    def mode(self):
        return self.MODES.index(self._mode)

    @mode.setter
    def mode(self, value):
        self._mode = self.MODES[value]
        self.changed.add("mode")

    @property  # mode_constant_x
    def mode_constant_x(self):
        return self._mode_constant_x

    @mode_constant_x.setter
    @constant
    def mode_constant_x(self, value):
        if type(value) == str:
            value = int(value)
        self._mode_constant_x = value
        self.changed.add("mode")

    @property  # mode_constant_y
    def mode_constant_y(self):
        return self._mode_constant_y

    @mode_constant_y.setter
    def mode_constant_y(self, value):
        if type(value) == str:
            value = int(value)
        self._mode_constant_y = value
        self.changed.add("mode")

    @property  # mode_motion_x
    def mode_motion_x(self):
        return self._mode_motion_x

    @mode_motion_x.setter
    @constant
    def mode_motion_x(self, value):
        if type(value) == str:
            value = int(value)
        self._mode_motion_x = value
        self.changed.add("mode")

    @property  # mode_motion_y
    def mode_motion_y(self):
        return self._mode_motion_y

    @mode_motion_y.setter
    def mode_motion_y(self, value):
        if type(value) == str:
            value = int(value)
        self._mode_motion_y = value
        self.changed.add("mode")

    @property  # mode_beacon_x
    def mode_beacon_x(self):
        return self._mode_beacon_x

    @mode_beacon_x.setter
    def mode_beacon_x(self, value):
        self._mode_beacon_x = value
        self.changed.add("mode")

    @property  # mode_beacon_y
    def mode_beacon_y(self):
        return self._mode_beacon_y

    @mode_beacon_y.setter
    @constant
    def mode_beacon_y(self, value):
        if type(value) == str:
            value = int(value)
        self._mode_beacon_y = value
        self.changed.add("mode")

    @property  # mode_hide_x
    def mode_hide_x(self):
        return self._mode_hide_x

    @mode_hide_x.setter
    @constant
    def mode_hide_x(self, value):
        self._mode_hide_x = value
        self.changed.add("mode")

    @property  # mode_hide_y
    def mode_hide_y(self):
        return self._mode_hide_y

    @mode_hide_y.setter
    @constant
    def mode_hide_y(self, value):
        self._mode_hide_y = value
        self.changed.add("mode")

    # Setup Data

    @property  # setup_data_in1
    def setup_data_in1(self):
        return self.__data["in1"]

    @setup_data_in1.setter
    def setup_data_in1(self, value):
        self.__data["in1"] = value
        self.changed.add("data")

    @property  # setup_data_in2
    def setup_data_in2(self):
        return self.__data["in2"]

    @setup_data_in2.setter
    def setup_data_in2(self, value):
        self.__data["in2"] = value
        self.changed.add("data")

    @property  # setup_data_out
    def setup_data_out(self):
        return self.__data["out"]

    @setup_data_out.setter
    def setup_data_out(self, value):
        self.__data["out"] = value
        self.changed.add("data")

    @property  # setup_data_acc
    def setup_data_acc(self):
        return self.__data["acc"]

    @setup_data_acc.setter
    def setup_data_acc(self, value):
        self.__data["acc"] = value
        self.changed.add("data")

    @property  # setup_data_gsm
    def setup_data_gsm(self):
        return self.__data["gsm"]

    @setup_data_gsm.setter
    def setup_data_gsm(self, value):
        self.__data["gsm"] = value
        self.changed.add("data")

    @property  # setup_data_dut1t
    def setup_data_dut1t(self):
        return self.__data["dut1t"]

    @setup_data_dut1t.setter
    def setup_data_dut1t(self, value):
        self.__data["dut1t"] = value
        self.changed.add("data")

    @property  # setup_data_dut2t
    def setup_data_dut2t(self):
        return self.__data["dut2t"]

    @setup_data_dut2t.setter
    def setup_data_dut2t(self, value):
        self.__data["dut2t"] = value
        self.changed.add("data")

    @property  # setup_data_adc1
    def setup_data_adc1(self):
        return self.__data["adc1"]

    @setup_data_adc1.setter
    def setup_data_adc1(self, value):
        self.__data["adc1"] = value
        self.changed.add("data")

    @property  # setup_data_pwrext
    def setup_data_pwrext(self):
        return self.__data["pwrext"]

    @setup_data_pwrext.setter
    def setup_data_pwrext(self, value):
        self.__data["pwrext"] = value
        self.changed.add("data")

    @property  # setup_data_pwrbat
    def setup_data_pwrbat(self):
        return self.__data["pwrbat"]

    @setup_data_pwrbat.setter
    def setup_data_pwrbat(self, value):
        self.__data["pwrbat"] = value
        self.changed.add("data")

    @property  # setup_data_oper
    def setup_data_oper(self):
        return self.__data["oper"]

    @setup_data_oper.setter
    def setup_data_oper(self, value):
        self.__data["oper"] = value
        self.changed.add("data")

    @property  # setup_data_dut1L
    def setup_data_dut1L(self):
        return self.__data["dut1L"]

    @setup_data_dut1L.setter
    def setup_data_dut1L(self, value):
        self.__data["dut1L"] = value
        self.changed.add("data")

    @property  # setup_data_dut2L
    def setup_data_dut2L(self):
        return self.__data["dut2L"]

    @setup_data_dut2L.setter
    def setup_data_dut2L(self, value):
        self.__data["dut2L"] = value
        self.changed.add("data")

    @property  # PIN
    def PIN(self):
        return self._PIN

    @PIN.setter
    def PIN(self, value: str):
        try:
            _ = int(value)
            assert len(value) == 4
        except (ValueError, AssertionError):
            raise ValidationError('Wrong PIN. "%s" can' "t be using as PIN" % value)
        self._PIN = value
        self.changed.add("PIN")

    @property  # info_ver
    def info_ver(self):
        if not self._info_ver:
            self._info_ver = self._get_info_ver()
        return self._info_ver

    # Admin Phone Numbers
    @property  # admin_phone_1
    def admin_phone_1(self):
        return self.__admin_phone[1]

    @admin_phone_1.setter
    def admin_phone_1(self, value):
        self.__admin_phone[1] = value or "*"
        self.changed.add("wl:pn1")

    @property  # admin_phone_2
    def admin_phone_2(self):
        return self.__admin_phone[2]

    @admin_phone_2.setter
    def admin_phone_2(self, value):
        self.__admin_phone[2] = value or "*"
        self.changed.add("wl:pn2")

    @property  # admin_phone_3
    def admin_phone_3(self):
        return self.__admin_phone[3]

    @admin_phone_3.setter
    def admin_phone_3(self, value):
        self.__admin_phone[3] = value or "*"
        self.changed.add("wl:pn3")

    # Spy Smart Tracking Calendar Mode
    @property  # mode_calendar_wake_1
    def mode_calendar_wake_1(self):
        return self._mode_calendar_wake[0]

    @mode_calendar_wake_1.setter
    def mode_calendar_wake_1(self, value):
        self._mode_calendar_wake[0] = value
        self.changed.add("mode")

    @property  # mode_calendar_wake_2
    def mode_calendar_wake_2(self):
        return self._mode_calendar_wake[1]

    @mode_calendar_wake_2.setter
    def mode_calendar_wake_2(self, value):
        self._mode_calendar_wake[1] = value
        self.changed.add("mode")

    @property  # mode_calendar_wake_3
    def mode_calendar_wake_3(self):
        return self._mode_calendar_wake[2]

    @mode_calendar_wake_3.setter
    def mode_calendar_wake_3(self, value):
        self._mode_calendar_wake[2] = value
        self.changed.add("mode")

    @property  # mode_calendar_wake_1_enabled
    def mode_calendar_wake_1_enabled(self):
        return self._mode_calendar_wake_enabled[0]

    @mode_calendar_wake_1_enabled.setter
    def mode_calendar_wake_1_enabled(self, value):
        self._mode_calendar_wake_enabled[0] = value
        self.changed.add("mode")

    @property  # mode_calendar_wake_2_enabled
    def mode_calendar_wake_2_enabled(self):
        return self._mode_calendar_wake_enabled[1]

    @mode_calendar_wake_2_enabled.setter
    def mode_calendar_wake_2_enabled(self, value):
        self._mode_calendar_wake_enabled[1] = value
        self.changed.add("mode")

    @property  # mode_calendar_wake_3_enabled
    def mode_calendar_wake_3_enabled(self):
        return self._mode_calendar_wake_enabled[2]

    @mode_calendar_wake_3_enabled.setter
    def mode_calendar_wake_3_enabled(self, value):
        self._mode_calendar_wake_enabled[2] = value
        self.changed.add("mode")


    def connect(self, port):

        # Перед подключением к новому трекеру сбросим данные которые касаются только подключенного сейчас трекера
        self.load_defaults_info()
        self.ser = serial.Serial(
            port, 115200, timeout=1, parity="N", stopbits=1, bytesize=8, xonxoff=0
        )
        self.ser.setDTR = None
        self.ser_io = io.TextIOWrapper(
            io.BufferedRWPair(self.ser, self.ser, 1),
            # newline='\n',
            line_buffering=True,
        )

        self.connected = True
        # Буфер для потока данных с порта
        self.read_buffer = []
        self.read_thread = threading.Thread(target=self.read_from_port)
        self.read_thread.start()

        # Если трекера по данному порту нет
        try:
            if self.info_ver[:1] != "V":
                raise IndexError
        except IndexError:
            self.disconnect()
            raise ConnectionError(
                "Нет связи с устройством. Возможно устройство не подключено или выбран неверный порт"
            )

    def disconnect(self):
        if self.connected:
            self.connected = False
            self.ser.close()

    def read_from_port(self):
        while True:
            if not self.connected:
                return
            response = self.ser.readline().decode("utf-8").strip()
            if response:
                print("<== %s" % response)
                logger.info(response)
                self.read_buffer.append(response)

    def send_cmd(self, command, wait=WAIT_DELAY, retries=10):
        # self.read_from_port()
        #  Сбросим буфер
        # self.ser.reset_input_buffer()
        while self.read_buffer:
            logger.info(self.read_buffer.pop(0))
        print("==> %s" % command)
        logger.info(command)
        # TODO: переписать через итераторы
        if self.debug:
            result = self.DEBUG_GET.get(command)
            logger.info(result)
            return result
        self.ser.write(crc(command))
        print("Sleep")
        sleep(wait)
        all_response = []
        for _ in range(self.ANSWER_LINES.get(command, 1)):
            response = ""
            timeout = 0
            while not response:
                try:
                    response = self.read_buffer.pop(0)
                except IndexError:
                    print("Long Sleep")
                    timeout += 1
                    if timeout == retries:
                        raise IndexError
                    sleep(wait)
            all_response.append(response)
        return all_response

    def _get_info_ver(self):
        return self.send_cmd("info:ver")[0]

    def get_info_gsm(self):
        return self.send_cmd("atcmd:at+csq")[1].split(": ")[-1]

    def get_info_gps(self):
        """Вычитываем статус GPS и разбираем его
        
        Returns:
            TYPE: Словарь {D: Время, G: Координаты, S: H:}
        """
        gps = self.send_cmd("info:gps")[0]
        gps = re.split(" (?=.=)", gps)
        gps = dict([i.split("=") for i in gps])
        return gps

    def get_info_battery(self):
        return int(self.send_cmd("batcharge")[0].split(":")[-1].split(",")[0])

    def get_info_interfaces(self):
        return self.send_cmd("info:sens")[0]

    def get_info_dut(self):
        return self.send_cmd("info:dut")[0]

    def send_ussd(self, ussd):
        return self.send_cmd("ussd:%s" % ussd, 5)[0]

    def update_firmware(self, url):
        # TODO: Add checking if URL is valid
        url = re.sub("^(softup:)", "", url)
        self.send_cmd("softup:" + url, 10)

    def reset_buffer(self):
        return self.send_cmd("reset:b", WAIT_RESET_DELAY)

    def restart(self):
        return self.send_cmd("reset:s", WAIT_RESET_DELAY)

    def disable_battery(self):
        return self.send_cmd("reset:l", WAIT_RESET_DELAY)

    def reset(self):
        return self.send_cmd("reset:f", WAIT_RESET_DELAY)

    def read_all(self):
        config = dict([i.split(": ") for i in self.send_cmd("info:conf")])
        self.active_sim = int(config["SIM"])
        self.server_ip = config["Server"].split(":")[0]
        self.server_port = config["Server"].split(":")[1]
        try:
            self.sim1_apn, self.sim1_log, self.sim1_pwd = config["apn1"].split(",")
        except ValueError:
            self.sim1_apn, self.sim1_log, self.sim1_pwd = [config["apn1"], "", ""]
        try:
            self.sim2_apn, self.sim2_log, self.sim2_pwd = config["apn2"].split(",")
        except ValueError:
            self.sim2_apn, self.sim2_log, self.sim2_pwd = [config["apn2"], "", ""]

        timeget = config["Timeget"].split(",")
        self.send_interval = int(timeget[0])
        self.send_distance = int(timeget[1])
        self.send_degree = int(timeget[2])
        self.send_engine = bool(int(timeget[3][0]))
        self.send_digital_input = bool(int(timeget[3][1]))
        self.send_speed = bool(int(timeget[3][2]))
        self.send_fuel = bool(int(timeget[3][3]))

        parking = config["Parking"].split(",")
        self.park_sensivity = int(parking[0])
        self.park_downtime = int(parking[1])
        self.sleep_downtime = int(parking[2])
        self._park_interval = int(parking[3])

        setup = config["Setup"].split(",")
        self.setup_keep_alive = int(setup[1])
        setup = dict(enumerate(setup[0]))

        # TODO: Обсудить что делать со старыми прошивками
        self.setup_autoreboot = bool(int(setup.get(5, 0)))
        self.setup_GSM_location = bool(int(setup.get(2, 0)))
        self.setup_kalman = bool(int(setup.get(4, 0)))
        self.setup_buffer = int(setup.get(3, 0))
        print(self.setup_buffer)
        self.PIN = self.send_cmd("pin:")[0].strip(";").split(":")[-1]

        self.interfaces_out = self.send_cmd("out:")[0].split("=")[-1]

        for i in self.send_cmd("data:")[0].strip(";").split(":")[-1].split(","):
            self.__data[i] = True

        if "mode" in self.allowed_commands:
            response = self.send_cmd("mode:")[0].strip(";")
            _, params = response.split(":", 1)
            self._mode, params = params.rsplit(":", 1)
            if self.mode == 0:
                self.mode_constant_x, self.mode_constant_y = params.split(",")
            elif self.mode == 1:
                self.mode_motion_x, self.mode_motion_y = params.split(",")
            elif self.mode == 2:
                self.mode_beacon_x, self.mode_beacon_y = params.split(",")
            elif self.mode == 3:
                self.mode_hide_x = int(params.split(",")[0])
            elif self.mode == 4:
                times = params.split(",")[:-1]
                enabled = [True] * len(times)
                times = times + ['00/00'] * (3 - len(times))
                enabled = enabled + [False] * (3 - len(enabled))
                self._mode_calendar_wake = [
                    datetime.strptime(t, "%H/%M").time() for t in times
                ]
                self._mode_calendar_wake_enabled = enabled

        if "wl:pn1" in self.allowed_commands:
            for i in [1, 2, 3]:
                self.__admin_phone[i] = self.send_cmd(f"wl:pn{i}:;")[0].split(":")[-1]

        if "bluetooth" in self.allowed_commands:
            self._bluetooth = self.send_cmd("bt:")[0].split(":")[-1] == "1"

        if "sim" in self.allowed_commands:
            res = self.send_cmd("sim:")[0].split(":")[-1].split(",")
            self._SIM_autoswitch = res[0] == "1"
            self._SIM_autoswitch_onfail_value = int(res[1])
            self._SIM_autoswitch_back_value = int(res[2])

        # Clean Changed
        self.changed = set()

    def write_all(self):
        for param in self.allowed_commands:
            if param not in self.changed:
                continue
            if param == "active_sim":
                cmd = self.send_cmd("reset:c" + str(self.active_sim), WAIT_RESET_DELAY)
                continue
            elif param == "server":
                cmd = ":".join(["server", self.server_ip, self.server_port])
            elif param == "sim1":
                cmd = (
                    "apn1:"
                    + ",".join([self.sim1_apn, self.sim1_log, self.sim1_pwd]).strip(",")
                    + (";" if self.sim1_log or self.sim1_pwd else "")
                )
            elif param == "sim2":
                cmd = (
                    "apn2:"
                    + ",".join([self.sim2_apn, self.sim2_log, self.sim2_pwd]).strip(",")
                    + (";" if self.sim2_log or self.sim2_pwd else "")
                )
            elif param == "timeget":
                cmd = "timeget:" + ",".join(
                    [
                        str(self.send_interval),
                        str(self.send_distance),
                        str(self.send_degree),
                        "".join(
                            [
                                str(int(self.send_engine)),
                                str(int(self.send_digital_input)),
                                str(int(self.send_speed)),
                                str(int(self.send_fuel)),
                            ]
                        ),
                    ]
                )
            elif param == "parking":
                cmd = "parking:" + ",".join(
                    [
                        str(self.park_sensivity),
                        str(self.park_downtime),
                        str(self.sleep_downtime),
                        str(self.park_interval),
                    ]
                )
            elif param == "setup":
                cmd = "setup:" + ",".join(
                    [
                        "".join(
                            [
                                "0",
                                "0",
                                str(int(self.setup_GSM_location)),
                                str(self.setup_buffer),
                                str(int(self.setup_kalman)),
                                str(int(self.setup_autoreboot)),
                            ]
                        ),
                        str(self.setup_keep_alive),
                    ]
                )
            elif param == "PIN":
                cmd = "pin:" + str(self.PIN)
            elif param == "out":
                cmd = "out:%s" % self.interfaces_out
            elif param == "data":
                cmd = (
                    "data:%s" % ",".join([k for k, v in self.__data.items() if v])
                    or "empty"
                )
            elif param == "mode":
                if self.mode == 0:
                    cmd = f"mode:{self._mode}:{self.mode_constant_x},{self.mode_constant_y};"
                elif self.mode == 1:
                    cmd = (
                        f"mode:{self._mode}:{self.mode_motion_x},{self.mode_motion_y};"
                    )
                elif self.mode == 2:
                    cmd = (
                        f"mode:{self._mode}:{self.mode_beacon_x},{self.mode_beacon_y};"
                    )
                elif self.mode == 3:
                    cmd = f"mode:{self._mode}:{self.mode_hide_x},{self.mode_hide_y};"
                elif self.mode == 4:
                    times = ",".join(
                        # [i.strftime("%H/%M") for i in self.__mode_calendar_wake]
                        [i[1].strftime("%H/%M") for i in zip(self._mode_calendar_wake_enabled, self._mode_calendar_wake) if i[0]]
                    )
                    cmd = f"mode:{self._mode}:{times},300;"

            elif param in ["wl:pn1", "wl:pn2", "wl:pn3"]:
                number = param[-1]
                cmd = f"wl:pn{number}:{self.__admin_phone[int(number)]};"
            elif param == "bluetooth":
                cmd = f"bt:{int(self._bluetooth)};"
            elif param == "sim:en":
                cmd = f"{param}:{int(self._SIM_autoswitch)};"
            elif param == "sim:set":
                try:
                    back = self._SIM_autoswitch_back_value
                except AttributeError:
                    back = 0
                cmd = f"{param}:{self._SIM_autoswitch_onfail_value},{back};"
            if not self.debug:
                self.send_cmd(cmd)
            else:
                logger.info(cmd)
            sleep(WAIT_BETWEEN_WRITE)
        self.changed = set()

    def save_to_file(self, filename):
        data = {}
        for field in self.FIELDS_TO_SAVE:
            try:
                data[field] = getattr(self, field)
            except AttributeError:
                continue
        json.dump(data, open(filename, "w", encoding="utf-8"))

    def load_from_file(self, filename):
        data = json.load(open(filename, "r", encoding="utf-8"))
        for k, v in data.items():
            setattr(self, k, v)


if __name__ == "__main__":
    print(show_serial_ports())
    test = Tracker()
    test.connect("COM1")
    # print(test.set_cmd('reset:cx'))
