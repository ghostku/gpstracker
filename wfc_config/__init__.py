# pylint: disable=no-name-in-module
import sys
import logging

from PyQt5.QtWidgets import QApplication

__app_name__ = "CarGO WFC Config Tool"
__version__ = "V2020-05-22.1"
_debug_ = False

try:
    __model__ = sys.argv[1].lower()
except IndexError:
    __model__ = "normal"
app = QApplication(sys.argv)
app.setApplicationVersion(__version__)
app.setApplicationName(__app_name__)

# Init logging
from wfc_config.log import QTextBrowserLogger, WFCLogFormatter, WFCLogger

logging.setLoggerClass(WFCLogger)
logger = logging.getLogger("WFCLogger")

from wfc_config.design import MainWindow
from wfc_config.hard import Tracker
from wfc_config.design_normal import NormalWindow
from wfc_config.design_spy import SpyWindow

tracker = Tracker(__model__, debug=_debug_)
if __model__ == "normal":
    ex = NormalWindow(tracker, __model__, debug=_debug_)
elif __model__ == "spy":
    ex = SpyWindow(tracker, __model__, debug=_debug_)


logger.addHandler(ex.get_logger())
logger.setLevel(logging.DEBUG)
# Exception logger
exlogger = logging.getLogger("exlogger")
# Configure logger to write to a file...
def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    exlogger.critical(
        "Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback)
    )


sys.excepthook = handle_exception

fh = logging.FileHandler("cargo.log")
formatter = logging.Formatter(
    "%(asctime)s - %(filename)s - %(funcName)s - %(name)s - %(levelname)s - %(message)s"
)
fh.setFormatter(formatter)
exlogger.addHandler(fh)

ex.show()
ex.show_update_info()
