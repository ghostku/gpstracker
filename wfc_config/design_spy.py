from wfc_config.design import MainWindow


class SpyWindow(MainWindow):
    STRINGS = {
        "title": " CarGo Spy&Unit Config Tool",
        "btn_info_interfaces": "Опрос АКБ",
    }

    def __init__(self, tracker, model, debug):
        super().__init__(tracker, model, debug, "./wfc_config/TrackerSpy.ui")

        # Admin phone numbers
        self.txt_admin_phone_1.editingFinished.connect(self.set_data)
        self.txt_admin_phone_2.editingFinished.connect(self.set_data)
        self.txt_admin_phone_3.editingFinished.connect(self.set_data)

        # SPY Smart Tracking
        self.btn_mode_always_on.toggled.connect(
            lambda x: self.tabs_mode.setCurrentIndex(0)
        )
        self.btn_mode_motion.toggled.connect(
            lambda x: self.tabs_mode.setCurrentIndex(1)
        )
        self.btn_mode_beacon.toggled.connect(
            lambda x: self.tabs_mode.setCurrentIndex(2)
        )
        self.btn_mode_hide.toggled.connect(
            lambda x: self.tabs_mode.setCurrentIndex(3)
        )
        self.btn_mode_calendar.toggled.connect(
            lambda x: self.tabs_mode.setCurrentIndex(4)
        )

        self.time_mode_calendar_wake_1.timeChanged.connect(self.set_data)
        self.time_mode_calendar_wake_2.timeChanged.connect(self.set_data)
        self.time_mode_calendar_wake_3.timeChanged.connect(self.set_data)

        self.tabs_mode.tabBar().hide()

        # self.groupBox_PIN.setEnabled(False)
        self.tab_server_layout.insertWidget(1, self.PIN_and_Buffer_2)
        self.tab_server_layout.insertWidget(2, self.groupBox_setup_update)
        self.tabs.removeTab(self.tabs.indexOf(self.tab_service))
        self.tabs.removeTab(self.tabs.indexOf(self.tab_interfaces))
        self.tabs.removeTab(self.tabs.indexOf(self.tab_smart_tracking))

        self.groupBox_mode_layout.insertWidget(-1, self.check_setup_GSM_location)

        self.btn_info_battery.clicked.connect(self.get_battery_info)

        self.ACTIVE_WHEN_CONNECTED = [
            self.btn_read_all,
            self.btn_write_all,
            self.btn_setup_update,
            self.btn_reset_buffer,
            # self.btn_battery_off,
            self.btn_reset,
            self.btn_restart,
            self.btn_info_common,
            self.btn_send_command,
            self.btn_info_battery,
            self.txt_ussd,
            self.btn_ussd,
        ]

        # Modes
        self.btns_mode.buttonClicked.connect(self.set_data)

        self.btns_mode.setId(self.btn_mode_always_on, 0)
        self.btns_mode.setId(self.btn_mode_motion, 1)
        self.btns_mode.setId(self.btn_mode_beacon, 2)
        self.btns_mode.setId(self.btn_mode_hide, 3)
        self.btns_mode.setId(self.btn_mode_calendar, 4)

        # self.spin_mode_constant_x.valueChanged.connect(self.set_data)
        self.spin_mode_constant_y.valueChanged.connect(self.set_data)
        # self.spin_mode_motion_x.valueChanged.connect(self.set_data)
        self.spin_mode_motion_y.valueChanged.connect(self.set_data)
        # self.spin_mode_beacon_y.valueChanged.connect(self.set_data)
        self.spin_mode_hide_x.valueChanged.connect(self.set_data)

        self.groupBox_mode_1.setStyleSheet('QGroupBox:title {color: rgb(240, 15, 15);}')

        self.cmb_mode_beacon_x.addItem("20 минут", "0")
        self.cmb_mode_beacon_x.addItem("1 час", "1")
        self.cmb_mode_beacon_x.addItem("3 часа", "2")
        self.cmb_mode_beacon_x.addItem("5 часов 30 минут", "3")
        self.cmb_mode_beacon_x.addItem("11 часов 30 минут", "4")
        self.cmb_mode_beacon_x.addItem("23 часа 15 минут", "5")
        self.cmb_mode_beacon_x.addItem("39 часов", "6")
        self.cmb_mode_beacon_x.currentIndexChanged.connect(self.set_data)

        self.check_mode_calendar_wake_1_enabled.toggled.connect(self.set_data)
        self.check_mode_calendar_wake_2_enabled.toggled.connect(self.set_data)
        self.check_mode_calendar_wake_3_enabled.toggled.connect(self.set_data)


    def get_show_list(self):
        main_show_list = super().get_show_list()
        spy_show_list = [
            self.btns_mode,
            self.spin_mode_constant_y,
            self.spin_mode_motion_y,
            self.cmb_mode_beacon_x,
            self.spin_mode_hide_x,
            self.txt_admin_phone_1,
            self.txt_admin_phone_2,
            self.txt_admin_phone_3,
            self.time_mode_calendar_wake_1,
            self.time_mode_calendar_wake_2,
            self.time_mode_calendar_wake_3,
            self.check_mode_calendar_wake_1_enabled,
            self.check_mode_calendar_wake_2_enabled,
            self.check_mode_calendar_wake_3_enabled
        ]
        return spy_show_list + main_show_list

    def get_battery_info(self):
        battery = self.slow_function_run(
            self.tracker.get_info_battery, "Опрашиваем аккумулятор. Подождите...", ""
        )
        self.lbl_info_battery.setText(str(battery) + " %")
        self.lbl_info_battery.setStyleSheet(
            "QLabel { color : %s}" % "red" if battery < 50 else "black"
        )

