import sys
from datetime import time

sys.argv[1] = "normal"
from wfc_config.hard import Tracker

MOCK_DATA = {
    "info:conf": [
        "SIM: 1",
        "Server: 127.0.0.1:80",
        "apn1: dsfsdf",
        "apn2: apn2222",
        "Timeget: 60,100,9,1111",
        "Parking: 80,60,0,300",
        "Setup: 001101,200",
    ],
    "pin:": ["pin:0003;"],
    "out:": ["Out=0"],
    "data:": ["data:dut1t;"],
}


def test_read_constant_mode(monkeypatch):
    def mock_send_cmd(request):
        return MOCK_DATA.get(request, [])
    tracker = Tracker()
    MOCK_DATA['mode:'] = ['mode:constant:90,300;']
    monkeypatch.setattr(tracker, "send_cmd", mock_send_cmd)
    tracker.allowed_commands = ["mode"]
    tracker.read_all()


def test_read_hide_mode(monkeypatch):
    def mock_send_cmd(request):
        return MOCK_DATA.get(request, [])
    tracker = Tracker()
    MOCK_DATA['mode:'] = ['mode:hide:p:18,240;']
    monkeypatch.setattr(tracker, "send_cmd", mock_send_cmd)
    tracker.allowed_commands = ["mode"]
    tracker.read_all()
    assert tracker.mode_hide_x == 18


def test_read_calendar_mode(monkeypatch):
    def mock_send_cmd(request):
        return MOCK_DATA.get(request, [])
    tracker = Tracker()
    MOCK_DATA['mode:'] = ['mode:hide:t:0/20,5/0,18/0,300;']
    monkeypatch.setattr(tracker, "send_cmd", mock_send_cmd)
    tracker.allowed_commands = ["mode"]
    tracker.read_all()
    assert time(0,20) == tracker.mode_calendar_wake_1
    assert time(5,0) == tracker.mode_calendar_wake_2
    assert time(18,0) == tracker.mode_calendar_wake_3
