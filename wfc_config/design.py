# Comment
import argparse
import logging
import sys
import requests

from datetime import datetime, time

from PyQt5 import uic

from wfc_config.MyPyQt import (QCursor, QFileDialog)
from wfc_config.MyPyQt import (Qt, QObject, QThread, pyqtSignal)
from wfc_config.MyPyQt import (QApplication, QButtonGroup, QCheckBox, QComboBox, QLabel, QLineEdit, QMainWindow,
                               QSpinBox, QTextBrowser, QPixmap, QIcon, QPushButton, QTimeEdit)

# from hard import Tracker
from wfc_config import app, logger
from wfc_config.hard import ConnectionError
from wfc_config.hard import ValidationError
from wfc_config.hard import show_serial_ports
from wfc_config.log import QTextBrowserLogger, WFCLogFormatter
# from wfc_config.TrackerConfig_windows import Ui_MainWindow
VERSIONS = {
    'FIRMWARE': {
        'V02040518-861359030185407-C31026D1': 'http://ghostku.com/firm/blabla.bin',
        'V02071217-861359030371544-6B397BD1': 'http://ghostku.com/firm/blabla.bin',
        'V02100118-861359037496773-10A8FD9B': 'http://ghostku.com/firm/blabla.bin'
    },
    'SOFTWARE': {
        'V2018-05-07': 'http://ghostku.com/cargo/soft/2018-05-07/tools.exe'
    },
    'RELATIONS': {
        'V02040518-861359030185407-C31026D1': ['V2018-05-07'],
        'V02071217-861359030371544-6B397BD1': ['V2018-05-07'],
        'V02100118-861359037496773-10A8FD9B': ['V2018-05-07'],
        'V02040518-861359030185407-C31026D1': ['V2018-11-12'],
        'Debug Version': ['V2018-05-07']
    }
}

READ_GPS_ON_CONNECT = False
UPDATE_FILE_URL = 'http://cargo-ufc.com/update.txt'


class MainWindow(QMainWindow):
    def __init__(self, tracker, model, debug, ui):
        super().__init__()
        uic.loadUi(ui, self)
        self.MODEL = model
        # self.STRINGS = STRINGS[model]
        self.debug = debug

        # COM Ports
        self.show_com_ports()       
        self.btn_reload_serial_ports.clicked.connect(self.show_com_ports)

        # Init logging
        self.logger_widget = QTextBrowserLogger(self)
        self.logger_widget.setFormatter(WFCLogFormatter())
        self.vl_log.addWidget(self.logger_widget.widget)
        self.btn_clear_log = QPushButton(self)
        self.btn_clear_log.setText('Очистить')
        self.btn_clear_log.clicked.connect(self.logger_widget.widget.clear)
        self.vl_log.addWidget(self.btn_clear_log)


        self.setWindowTitle(self.STRINGS['title'] + ' ' + app.applicationVersion())
        self.lbl_logo.setPixmap(QPixmap('./wfc_config/images/logo_221.jpg'))
        self.setWindowIcon(QIcon('./wfc_config/images/icon.ico'))

        self.btn_connection.clicked.connect(self.connect_to_device)

        self.btn_read_all.clicked.connect(self.get_all)
        self.btn_write_all.clicked.connect(self.write_all)

        self.show_list = self.get_show_list()

        self.write_list = [
            self.check_setup_data_in1, self.check_setup_data_in2, self.check_setup_data_out, self.check_setup_data_acc,
            self.check_setup_data_gsm, self.check_setup_data_dut1t, self.check_setup_data_dut2t,
            self.check_setup_data_adc1, self.check_setup_data_pwrext, self.check_setup_data_pwrbat,
            self.check_setup_data_oper, self.check_setup_data_dut1L, self.check_setup_data_dut2L
        ]

        # Send Command
        self.btn_send_command.clicked.connect(self.send_command)
        self.txt_send_command.returnPressed.connect(self.send_command)

        # Control Buttons
        self.btn_reset_buffer.clicked.connect(self.reset_buffer)
        self.btn_restart.clicked.connect(self.restart)
        self.btn_battery_off.clicked.connect(self.disable_battery)
        self.btn_reset.clicked.connect(self.reset)

        # Info Buttons
        self.btn_info_common.clicked.connect(lambda: self.show_info(True))
        self.btn_ussd.clicked.connect(self.send_ussd)
        self.txt_ussd.returnPressed.connect(self.send_ussd)

        # Server
        self.txt_server_ip.editingFinished.connect(self.set_data)
        self.txt_server_port.editingFinished.connect(self.set_data)

        # SIM
        self.btns_active_sim.buttonClicked.connect(self.set_active_sim)
        for i, button in enumerate(self.btns_active_sim.buttons()):
            self.btns_active_sim.setId(button, i + 1)

        # Auto SIM switch
        self.spin_SIM_autoswitch_onfail.valueChanged.connect(lambda: self.spin_SIM_autoswitch_back.setEnabled(self.sender().minimum() != self.sender().value()))
        self.spin_SIM_autoswitch_onfail.valueChanged.connect(self.set_data)
        self.spin_SIM_autoswitch_back.valueChanged.connect(self.set_data)
        # self.check_SIM_autoswitch.clicked.connect(self.set_data)
        # self.spin_SIM_autoswitch_timeout.valueChanged.connect(self.set_data)
        # self.check_SIM_autoswitch.stateChanged.connect(lambda x: self.spin_SIM_autoswitch_timeout.setEnabled(self.check_SIM_autoswitch.isChecked()))

        # APNs
        self.txt_sim1_apn.editingFinished.connect(self.set_data)
        self.txt_sim1_log.editingFinished.connect(self.set_data)
        self.txt_sim1_pwd.editingFinished.connect(self.set_data)
        self.txt_sim2_apn.editingFinished.connect(self.set_data)
        self.txt_sim2_log.editingFinished.connect(self.set_data)
        self.txt_sim2_pwd.editingFinished.connect(self.set_data)

        # Timeget
        self.spin_send_interval.valueChanged.connect(self.set_data)
        self.spin_send_distance.valueChanged.connect(self.set_data)
        self.spin_send_degree.valueChanged.connect(self.set_data)
        self.check_send_engine.clicked.connect(self.set_data)
        self.check_send_digital_input.clicked.connect(self.set_data)
        self.check_send_speed.clicked.connect(self.set_data)
        self.check_send_fuel.clicked.connect(self.set_data)

        # Parking
        # self.spin_park_sensivity.valueChanged.connect(self.set_data)
        self.spin_park_downtime.valueChanged.connect(self.set_data)
        self.spin_sleep_downtime.valueChanged.connect(self.set_data)
        self.spin_park_interval.valueChanged.connect(self.set_data)

        # Setup
        # self.check_setup_autoreboot.clicked.connect(self.set_data)
        # self.check_setup_kalman.clicked.connect(self.set_data)
        self.check_setup_GSM_location.clicked.connect(self.set_data)
        self.txt_PIN.editingFinished.connect(self.set_data)
        # self.spin_setup_keep_alive.valueChanged.connect(self.set_data)
        self.check_setup_keep_alive.clicked.connect(self.set_data)

        self.btns_setup_buffer.buttonClicked.connect(self.set_data)
        self.btns_setup_buffer.setId(self.btn_setup_buffer_FIFO, 0)
        self.btns_setup_buffer.setId(self.btn_setup_buffer_LIFO, 1)

        self.btn_setup_update.clicked.connect(self.update_firmware)


        # Save & Load
        self.btn_save_file.clicked.connect(self.save_file)
        self.btn_load_file.clicked.connect(self.load_file)

        # Setup Data
        for obj in self.write_list:
            if type(obj) == QCheckBox:
                obj.clicked.connect(self.set_data)

        # Interfaces
        self.cmb_interfaces_out.addItem('Выключено', '0')
        self.cmb_interfaces_out.addItem('Включено', '1')
        self.cmb_interfaces_out.addItem('Блокировка', 'b')
        self.cmb_interfaces_out.currentIndexChanged.connect(self.set_data)

        self.show_list.append(self.cmb_bluetooth)
        self.cmb_bluetooth.addItem('Выключено', '0')
        self.cmb_bluetooth.addItem('Включено', '1')
        self.cmb_bluetooth.currentIndexChanged.connect(self.set_data)

        self.tracker = tracker

        # Set Limits
        # Parking
        self.spin_park_downtime.setRange(0, 86400)
        self.spin_sleep_downtime.setRange(0, 86400)
        self.spin_park_interval.setRange(0, 86400)

        # self.btn_info_battery.setEnabled(False)

        if self.MODEL == 'spy':
            pass





        elif self.MODEL == 'normal':
            self.tabs.removeTab(self.tabs.indexOf(self.tab_smart_tracking_spy))
        self.show_update_info()

    def get_show_list(self):
        return [
            self.btns_active_sim, self.txt_server_ip, self.txt_server_port, self.txt_sim1_apn, self.txt_sim1_log,
            self.txt_sim1_pwd, self.txt_sim2_apn, self.txt_sim2_log, self.txt_sim2_pwd, self.spin_send_interval,
            self.spin_send_distance, self.spin_send_degree, self.check_send_engine, self.check_send_digital_input,
            self.check_send_speed, self.check_send_fuel, self.spin_park_downtime,
            self.spin_sleep_downtime, self.spin_park_interval,
            self.check_setup_GSM_location, self.btns_setup_buffer, self.txt_PIN, self.check_setup_keep_alive,
            self.cmb_interfaces_out, self.check_setup_data_in1, self.check_setup_data_in2, self.check_setup_data_out,
            self.check_setup_data_acc, self.check_setup_data_gsm, self.check_setup_data_dut1t,
            self.check_setup_data_dut2t, self.check_setup_data_adc1, self.check_setup_data_pwrext,
            self.check_setup_data_pwrbat, self.check_setup_data_oper, self.check_setup_data_dut1L,
            self.check_setup_data_dut2L, 
            self.cmb_bluetooth, # bluetooth
            self.spin_SIM_autoswitch_onfail, self.spin_SIM_autoswitch_back, # SIM AutoSwitch
        ]
    
    def hide_all_except_one(self, hide_list, show_item=None):
        for item in hide_list:
            item.setHidden(not item is show_item)
        # if show_item:
        #     self.check_mode_GSM_location.setEnabled(True)
    def show_update_info(self):
        try:
            message = requests.get(UPDATE_FILE_URL).text
            logger.info(message)
        except requests.exceptions.ConnectionError:
            pass

    def show_com_ports(self):
        self.comboSelectSerialPort.clear()
        self.comboSelectSerialPort.addItems(show_serial_ports())


    def slow_function_run(self, myfunc, status, succes='', error='', *args):
        '''эта функция должна дизейблить все окно пока выполняется другая "долгая" функция
        TODO: Переписать как декоратор
        '''
        self.statusbar.showMessage(status)
        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        # if incase function failed then restore it
        result = None
        try:
            result = myfunc(*args)
            QApplication.restoreOverrideCursor()
            logger.info(succes)
        except Exception as e:
            QApplication.restoreOverrideCursor()
            if self.debug:
                print('we are here')
                raise(e)
            logger.error(error)
        self.statusbar.clearMessage()
        return result

    def get_logger(self):
        return self.logger_widget

    def connect_to_device(self):
        if self.tracker.connected:
            self.tracker.disconnect()
            self.btn_connection.setText('Подключиться')
        else:
            self.statusbar.showMessage('Подключаемся к трекеру. Подождите...')
            try:
                serial = self.comboSelectSerialPort.currentText()
                logger.info('Connecting to %s' % serial)
                self.tracker.connect(serial)
                # self.check_versions(VERSIONS) # Контроль версии прошивки
                self.btn_connection.setText('Отключить')
                self.show_info(READ_GPS_ON_CONNECT)
            except ConnectionError as e:
                logger.error(str(e))
        for button in self.ACTIVE_WHEN_CONNECTED:
            button.setEnabled(self.tracker.connected)
        self.statusbar.clearMessage()

    def show_info(self, read_gps=True):
        self.statusbar.showMessage('Обновляем информацию. Подождите...')
        self.lbl_firmware_ver.setText(self.tracker.info_ver)
        if read_gps:
            self.lbl_info_gsm.setText('Уровень сигнала: %s' % self.tracker.get_info_gsm())
            gps = self.slow_function_run(self.tracker.get_info_gps, 'Получение данных GPS', '',
                                         'Ошибка получения данных GPS')
            if gps:
                self.lbl_info_gps_gmap.setText(
                    '<a href="https://www.google.com.ua/maps/place/%s">Посмотреть на карте</a>' % gps['G'])
                gps = '\n'.join(['%s=%s' % (k, v) for k, v in gps.items()])
            self.lbl_info_gps.setText(gps)
        self.statusbar.clearMessage()

    def check_versions(self, versions):
        firmware = self.tracker.info_ver
        software = app.applicationVersion()
        try:
            if software in versions['RELATIONS'][firmware]:
                logger.info('Версия прошивки и программы совпадает')
            else:
                latest = versions['RELATIONS'][firmware][-1]
                logger.error(
                    'Версия прошивки и программы НЕ СОВПАДАЮТ. Работосопособность программы с данным устройством не гарантирована. Для данной версии прошивки рекомендуется программа %s которую вы можете скачать %s'
                    % (latest, versions['SOFTWARE'][latest]))
        except KeyError:
            logger.error(
                'Версия прошивки и программы НЕ СОВПАДАЮТ. Работосопособность программы с данным устройством не гарантирована.'
            )

    def get_all(self):
        self.slow_function_run(
            self.tracker.read_all, 'Считываем информацию с трекера. Подождите...', 'Настройки трекера считаны',
            'Во время считывания данных произошла ошибка. Возможно устройство занято, подождите и попробуйте еще раз')
        self.show_all()

    def show_item(self, obj):
        name = obj.objectName().split('_', 1)[1]
        obj.blockSignals(True)
        if name in ['SIM_autoswitch_onfail', 'SIM_autoswitch_back']: # Костыль для выделения особых spinBox
            if getattr(self.tracker, name + '_enabled'):
                obj.setValue(getattr(self.tracker, name + '_value'))
            else:
                obj.setValue(obj.minimum())
        elif type(obj) == QLineEdit:
            obj.setText(getattr(self.tracker, obj.objectName()[4:]))
        elif type(obj) == QButtonGroup:
            obj.button(getattr(self.tracker, name)).setChecked(True)
        elif type(obj) == QSpinBox:
            obj.setValue(getattr(self.tracker, name))
        elif type(obj) == QCheckBox:
            obj.setChecked(getattr(self.tracker, name))
        elif type(obj) == QComboBox:
            obj.setCurrentIndex(obj.findData(getattr(self.tracker, name)))
        elif type(obj) == QTimeEdit:
            obj.setTime(getattr(self.tracker, name))
        obj.blockSignals(False)

    def show_all(self):
        for obj in self.show_list:
            self.show_item(obj)

    def write_all(self):
        self.statusbar.showMessage('Записываем настройки в трекер. Подождите...')
        try:
            self.tracker.write_all()
        except ValidationError as e:
            logging.error(str(e))
        finally:
            self.statusbar.clearMessage()

    # def log(self, message, style='INFO'):
    #     colors = {'INFO': 'black', 'OK': 'green', 'ERROR': 'red'}
    #     self.consoleOutput.append('<p style="color:%s">%s</p>' % (colors[style], message))

    def set_data(self):
        obj = self.sender()
        name = obj.objectName().split('_', 1)[1]
        try:
            if name == 'PIN':  # Костыль для PIN
                if(len(obj.text()) < 4):
                    txt = obj.text()
                    obj.setText('0' * (4 - len(txt)) + txt)
            if name in ['SIM_autoswitch_onfail', 'SIM_autoswitch_back']: # Костыль для выделения особых spinBox
                enable = obj.minimum() != obj.value()
                setattr(self.tracker, name + '_enabled', enable)
                if enable:
                    setattr(self.tracker, name + '_value', obj.value())
            elif type(obj) == QLineEdit:
                setattr(self.tracker, name, obj.text())
            elif type(obj) == QCheckBox:
                setattr(self.tracker, name, obj.isChecked())
            elif type(obj) == QSpinBox:
                setattr(self.tracker, name, obj.value())
            elif type(obj) == QButtonGroup:
                setattr(self.tracker, name, obj.checkedId())
            elif type(obj) == QComboBox:
                setattr(self.tracker, name, obj.itemData(obj.currentIndex()))
            elif type(obj) == QTimeEdit:
                dt = time(obj.time().hour(), obj.time().minute())
                setattr(self.tracker, name, dt)
        except ValidationError as e:
            # self.log(str(e))
            logger.error(str(e))
            self.show_item(obj)
            # raise

    def set_active_sim(self):
        self.tracker.active_sim = self.btns_active_sim.checkedId()

    def set_sim1_apn(self):
        self.tracker.sim1_apn = self.txtAPN1.text()

    def set_sim1_log(self):
        self.tracker.sim1_log = self.txtAPN1Log.text()

    def set_sim1_pwd(self):
        self.tracker.sim1_pwd = self.txtAPN1Pwd.text()

    def set_sim2_apn(self):
        self.tracker.sim2_apn = self.txtAPN2.text()

    def set_sim2_log(self):
        self.tracker.sim2_log = self.txtAPN2Log.text()

    def set_sim2_pwd(self):
        self.tracker.sim2_pwd = self.txtAPN2Pwd.text()

    def update_firmware(self):
        # TODO: Проверка на валидный URL
        self.tracker.update_firmware(self.txt_setup_update.text())

    def reset_buffer(self):
        self.slow_function_run(self.tracker.reset_buffer, 'Сброс буфера. Подождите...', 'Буфер сброшен')

    def restart(self):
        self.slow_function_run(self.tracker.restart, 'Перезапуск устройства. Подождите...', 'Устройство перезапущено')

    def reset(self):
        self.slow_function_run(self.tracker.reset, 'Сброс настроек устройства. Подождите...', 'Устройство сброшено')

    def disable_battery(self):
        self.slow_function_run(self.tracker.disable_battery, 'Отключаем аккумулятор. Подождите...',
                               'Аккумулятор отключен')

    def send_ussd(self):
        result = self.slow_function_run(self.tracker.send_ussd, 'Отправили USSD-команду. Ждем ответа', '',
                                        'Нет ответа на USSD-запрос', self.txt_ussd.text())
        # logger.info('Отправили USSD-команду. Ждем ответа')
        logger.info(result)

    def send_command(self):
        command = self.txt_send_command.text()
        self.slow_function_run(self.tracker.send_cmd, 'Послали команду', '', '', command)

    def save_file(self):
        filename, _ = QFileDialog.getSaveFileName(self, 'Сохранение настроек')
        if filename:
            self.tracker.save_to_file(filename)
            logger.info('Настройки сохранены')

    def load_file(self):
        filename, _ = QFileDialog.getOpenFileName(self, 'Загрузка настроек')
        if filename:
            self.tracker.load_from_file(filename)
            self.show_all()
            logger.info('Настройки загружены')

    def get_interfaces_info(self):
        self.slow_function_run(self.tracker.get_info_interfaces, 'Опрашиваем интерфейсы. Подождите...', '')

    def get_dut_info(self):
        self.slow_function_run(self.tracker.get_info_dut, 'Опрашиваем ДУТ. Подождите...', '')

    def closeEvent(self, event):
        self.tracker.disconnect()
        event.accept()


if __name__ == '__main__':
    pass
    # parser = argparse.ArgumentParser(description='GPS Tracker Config')
    # parser.add_argument(
    #     '--debug',
    #     '-d',
    #     action='store_true',
    #     required=False,
    #     help='Enable debug mode',
    # )
    # params = parser.parse_args()

    # app = QApplication(sys.argv)
    # # tracker = Tracker(params.debug)
    # # ex = MainWindow(tracker)
    # ex.show()
    # sys.exit(app.exec_())
